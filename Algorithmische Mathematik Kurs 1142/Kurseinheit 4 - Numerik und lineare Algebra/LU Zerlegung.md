---
tags: KE_4 LU-Zerlegung
---

## LU Zerlegung
### Definitionen
- [[obere Dreiecksmatrix]]
- [[untere Dreiecksmatrix]]
- [[Transpositionsmatrix]]
- [[Permutationsmatrix]]
- [[Frobeniusmatrix]]


$PA = LU$