---
tags: KE_4 LU-Zerlegung
---

## Obere Dreiecksmatrix

Sei $A$ eine $(n  n)$-Matrix.
Dann ist die obere Dreiecksmatrix, wenn unterhalb der Diagonalen nur Nullen stehen oder auch:

$A_{ij}= 0$ für alle $i>j$ und $1 \leq i, j \leq n$ ist.