
$A \in \mathbb{R}^{n n}$

Sei A symmetrisch und positiv definit. Dann existiert eine linke untere Dreiecksmatrix L mit $A = LL^\top$.

