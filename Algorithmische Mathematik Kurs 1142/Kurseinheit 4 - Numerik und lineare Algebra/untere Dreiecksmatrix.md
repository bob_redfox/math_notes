---
tags: KE_4 LU-Zerlegung
---

## Untere Dreiecksmatrix

Sei $A$ eine $(n  n)$-Matrix.
Dann ist die untere Dreiecksmatrix, wenn über der Diagonalen nur Nullen stehen oder auch:

$A_{ij}= 0$ für alle $i<j$ und $1 \leq i, j \leq n$ ist.