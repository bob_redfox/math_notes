
---
tags: KE_4 Numerik
---
## Zahlendarstellung im B-adischen System

Sei $x \in \mathbb{R} \backslash \{0\}$ und $B \in \mathbb{N},\ B\ge 2$ Dann gibt es genau eine Darstellung der Gestalt

$$x = \sigma B^n \sum_{i=1}^{\infty} x_{-i}B^{-i}$$

mit
- $\sigma \in \{+1,-1\}$
- $n\in \mathbb{Z}$
- $x_{-i}\in \{0,1,...,B-1\}$
- $x_{-i} \neq 0$
- $\forall j \in \mathbb{N}\exists k\ge j: x_{-k} \neq B-1$

Beispiel:

$143 = 1*10^2 + 4 * 10^1 + 3*10^0$