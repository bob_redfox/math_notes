---
tags: KE_4 Numerik lineare_Algebra Inhaltsverzeichnis
---
## Numerik und lineare Algebra

- [[Abgeschlossenes und offenes Interval]]
- [[Normen]]
- [[Kodierung von Zahlen]]
	- [[Fehlerquellen]]
- [[Gaußelimination]]
- [[Invertierbarkeit von Matrizen]]
- [[LU Zerlegung]]
- [[Gauß-Jordan-Algorithmus]]
- [[Cholesky-Faktorisierung]]