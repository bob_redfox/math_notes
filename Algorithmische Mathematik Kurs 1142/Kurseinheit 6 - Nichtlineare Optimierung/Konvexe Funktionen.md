---
tags: KE_5 Konvexität
---

## Konvexe Funktionen

### Eigenschaften
- $f(\lambda x + (1-\lambda)y) \le \lambda f(x) + (1-\lambda) f(y)$