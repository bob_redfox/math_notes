---
tags: KE_6, nichtlineare_Optimierung
---

## Verfahren zur Nichtlinearen Optimierung


- [[Das allgemeine Suchverfahren]]
    - [[Unimodalität]]
    - [[Konvexe Mengen]]
    - [[Konvexe Funktionen]]
    - [[Implementierung des allgemeinen Suchverfahrens]]
- [[Koordinatensuche]]
- [[Methode des steilsten Abstiegs]]
- [[Newtonverfahren]]
- [[Verfahren der konjugierten Richtungen]]
	- [[Verfahren nach Fletcher Reeves]] 