## Allgemein
- [[Griechische Buchstaben]]

## Kurseinheit 4
- Reguläre Matrix:
	- Eine quadratische Matrix die eine Inverse besitzt

- Positiv definit
	- Eine Matrix $A$ ist positiv (semi-) definit wenn $xAx^\top > 0$


## Kurseinheit 7
- Sei $A\in\mathbb{R^{m\times n}}$ und $I$ eine Menge.
	- $A_{I.}$ beschreibt die Zeilen mit Index aus der Menge $I$
	-  $A_{.I}$ beschreibt die Spalten mit Index aus der Menge $I$