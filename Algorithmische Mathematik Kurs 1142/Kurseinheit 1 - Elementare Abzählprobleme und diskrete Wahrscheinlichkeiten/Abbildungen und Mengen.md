---
tags: KE_1
---

## Abbildungen und  Mengen

Seien $n,m \in \mathbb{N}$

---
Sei $m\ge 1$ und $A$ eine n-elementige Menge und $R$ eine m-elementige Menge. Dann ist die Anzahl aller Abbildungen $f: A \rightarrow R$ gerade $m^n$ . (Proposition 2.1.2)

---
Sei $X$ eine n-elementige Menge, dann hat $X$ genau $2^n$ Teilmengen. (Korollar 2.1.3)
$$|2^X| = 2^{|X|}$$

---

Sei $n \ge 1$. Jede n-elementige Menge hat genau $2^{n-1}$ Teilmengen mit ungeraden vielen Elementen und ebenso viele mit geraden vielen Elementen.

### Abbildungen
- [[Urnenexperimente]]

