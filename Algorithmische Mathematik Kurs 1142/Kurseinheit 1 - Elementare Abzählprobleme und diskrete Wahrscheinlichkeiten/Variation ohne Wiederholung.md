---
tags: KE_1 Urnenexperimente
--- 

## Variation ohne Wiederholung

Ein Urnenexperiment, wo man die Anzahl der möglichen Sequenzen erfasst, wenn man n-Kugeln zieht, diese aber nach dem Ziehen nicht mehr zurücklegt.

Hierbei ist die Anzahl der Möglichen Abbildungen mit $n, m \in \mathbb{N}$:
$$\prod_{i=0}^{n-1}(m-i)$$

