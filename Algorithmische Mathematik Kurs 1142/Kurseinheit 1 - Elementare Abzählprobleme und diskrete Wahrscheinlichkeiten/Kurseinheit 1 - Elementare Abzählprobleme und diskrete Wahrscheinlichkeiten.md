---
tags: KE_1 Kombinatorik diskrete_Wahrscheinlichkeiten
---

## Elementare Abzählprobleme und diskrete Wahrscheinlichkeiten

- [[Abbildungen und Mengen]]
- [[Permutationen]]
- [[Binominalkoeffizienten]]
- [[Abschätzungen]]
- [[Das Prinzip von Inklusion und Exklusion]]