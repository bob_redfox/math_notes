---
tags: KE_1
---

## Abschätzungen 

Seien $f, g: \mathbb{N} \rightarrow \mathbb{R}$
Dann schreiben wir 
$$f = O(g)$$

oder

$$f(n) = O(g(n))$$
wenn es eine Konstante C und einen Startpunkt $n_1 \in \mathbb{N}$, so dass für alle $n \in \mathbb{N}, n\ge n_1$ gilt $|f(n)| \le Cg(n)$

Das ganze kann man auch "Big-O-Notation" nennen.

Zu beachten:
- Diese Abschätzung liefert nur eine Abschätzung nach oben, nicht nach unten.
- Die Relation ist nicht symmetrisch.

### Zusammenhänge für Abschätzungen
Seien $C, a, \alpha, \beta > 0$ feste reelle positive Zahlen unabhängig von n. Dann gilt:
a) $\alpha \le \beta$ => $n^{\alpha} = O(n^{\beta})$ 
b) $a > 1$ => $n^C = O(a^n)$ 
c) $\alpha > 0$ => $(ln\ n)^C = O(n^{\alpha})$