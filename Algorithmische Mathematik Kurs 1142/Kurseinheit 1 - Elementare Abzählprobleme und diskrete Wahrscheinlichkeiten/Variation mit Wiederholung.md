---
tags: KE_1 Urnenexperimente
---

## Variation mit Wiederholung

Ein Urnenexperiment wo, man die möglichen Sequenzen betrachtet, wenn man n-Kugeln aus einer Menge zieht und anschließend wieder in die Menge zurücklegt.

Hierbei ist die Anzahl der möglichen Sequenzen mit $n, m \in \mathbb{n}, m \ge 1$ :
$$m^n$$
