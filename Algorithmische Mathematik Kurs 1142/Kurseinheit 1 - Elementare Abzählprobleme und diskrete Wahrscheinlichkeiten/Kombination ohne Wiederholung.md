---
tags: KE_1 Urnenexperimente
---

## Kombination ohne Wiederholung

Bei diesem Urnenexperiment werden Kugeln ohne Zurücklegen gezogen und es wird am Ende die Reihenfolge der gezogenen Zahlen ignoriert.

Die Anzahl der Möglichkeiten dies zu berechnen erfolgt mittels [[Binominalkoeffizienten]].