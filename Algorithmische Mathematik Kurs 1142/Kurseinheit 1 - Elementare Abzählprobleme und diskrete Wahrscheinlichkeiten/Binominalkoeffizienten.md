---
tags: KE_1 todo
---

## Binomialkoeffizienten
Seien $n, k \in \mathbb{N}, n\ge k$

Der Binomialkoeffizient n über k ist wie folgt definiert:
$$\begin{pmatrix}
n\\
k
\end{pmatrix} = \frac{\prod_{i=0}^{k-1}(n-i)}{k!}$$

oder:

$$\begin{pmatrix}
n\\
k
\end{pmatrix} = \frac{n!}{k!(n-k)!}$$

Binomialkoeffizienten haben auch noch folgende Eigenschaften:

a) $$\begin{pmatrix}n\\k \end{pmatrix} = \begin{pmatrix}n\\n-k 
\end{pmatrix}$$
b) Seien zusätzlich $n\ge k\ge 1$. Dann ist:
$$\begin{pmatrix}n-1\\k-1 \end{pmatrix} + \begin{pmatrix}n-1\\k \end{pmatrix}=\begin{pmatrix}n\\k 
\end{pmatrix}$$