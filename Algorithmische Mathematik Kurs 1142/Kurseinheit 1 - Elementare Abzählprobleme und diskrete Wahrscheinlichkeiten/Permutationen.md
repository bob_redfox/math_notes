---
tags: KE_1 toDo
---

## Permutationen

Jede Permutation $\sigma$ lässt sich (bis auf die Reihenfolge eindeutig) in paarweise disjunkte Zyklen zerlegen.
