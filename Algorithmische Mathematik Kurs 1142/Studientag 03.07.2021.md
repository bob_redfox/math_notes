# Studientag 03.07.2021

## [[Kurseinheit 4 - Numerik und lineare Algebra]]

- Kodierung von Zahlen
- [[Lineare Gleichungssysteme]]
	- [[Gaußsche Eliminationsverfahren]]
		Lösungsmenge eines LG ändert sich nicht, wenn man in der erweiterten Koeffizientenmatrix (A|b)
			- Zeilen vertauscht
			- Zeilen mit Skalaren multipliziert
			- das Vielfache einer Zeile zu einer anderen addiert
		
	- [[Gauß-Eliminationsschritt]]
	- [[Gauß-Jordan-Eliminationsschritt]]
		- [[Gauß-Jordan-Algorithmus]]
- [[LU Zerlegung]]
	- Betrachten von quadratischen Matrizen von vollem Rang
		- Jede reguläre Matrix A gibt es eine Dreieckszerlegeung PA = LU wobei L linkere untere und U rechte obere Dreiecksmatrix ist und P Permutationsmatrix
		Schritte zum Lösen Ax=b:
			- Pb = PAx = L(Ux)
			- Löse Ly = Pb
			- Löse UX = y
- [[Abschätzung des Rechenaufwandes]]
- [[Cholesky-Faktorisierung]]
- [[Fehlerfortpflanzung]]
	- Normen reeler Vekorräumen
		- Eine Abbildung $\mid\mid *  \mid\mid$ : $V \rightarrow \mathbb{R}$  heißt Norm auf V.
		- 1 - Norm $\mid\mid 1 \mid\mid_1 = \sum_{i=1}^n\mid x_1 \mid$
		- Spaltensummenorm
		- Zeilensummernorm
		- euklidische Norm
			- Spektralnorm
	- Fehlerabschätzung 
		- exakter Wert
		- Näherungswert
		- absoluter Fehler
		- relativer Fehler
		- Kondition einer Matrix
	- Fehlerfortpflanzung
	- Störung von Gleichungssystemen Ax = b => $(A + \Delta A)(x + \Delta x) = b$ etc.

Klausur? 
- Formel Summe der Quadrate
- Fehlerfortpflanzung Satz 2
- Hadamar Matritze

## [[Kurseinheit 5 - Nichlineare Optimierung]]

- Die allgemeine Optimierungsaufgabe
	Minimiere $f(x)$ über alle $x \in Z$, wobei $Z$ eine Menge ist.
	- f sollte stetig, besser (zweimal) stetig differenzierbar sein
	- Z sollte zusammenhängend sein
	- etc.
	Schreibweise
	Min $f(x)$
	u.d.N. $h_j(x) = 0$
	$g_j(x) \le 0$
- Analysis
- Unrestringierte Optimierung
	- m = l = 0
- Restringierte Optimierung
- lokale und globale Lösungen
- Spezialfälle Satz von Taylor
- Notwendige, bzw. hinreichende Kriterien
	- x* lokale Minimalstelle => f'(x) = 0
	- $x^*$ lokale Minimalstelle => $f'(x^*) = 0$ und $f''(x^*) \ge 0$
- Richtungsableitung
	- Im eindimensionalen können wir nur in eine Richtung Ableitung
	- Im mehrdimensionalen Fall können wir in verschiende Richtungen ableiten
	- i-te [[partielle Ableitung]]
		- Leite nur nach $x_i$ ab, die anderen Variablen sind konstant
	-(j,i)-te [[partielle Ableitung]]
	- [[Gradienten]]
	- [[Hessematrix]]
	- Abstiegsrichtungen
	- Gleichungsdefinierte Mannigfaltigkeiten
		- [[Mannigfaltigkeiten]]
		- Vektoren die tangential zu der Mannigfaltigkeit verlaufen nennen wir den Tangentialraum.
- Kuhn-Tucker-Bedingungen