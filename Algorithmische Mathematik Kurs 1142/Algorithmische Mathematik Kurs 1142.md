---
tags: Inhalt
---
## Algorithmische Mathematik Kurs 1142 Stand: SS 2021

-[[Kurseinheit 1 - Elementare Abzählprobleme und diskrete Wahrscheinlichkeiten]]
-[[Kurseinheit 2 - Graphen]]
-[[Kurseinheit 3 - Bäume und Matchings]]
-[[Kurseinheit 4 - Numerik und lineare Algebra]]
-[[Kurseinheit 5 - Nichlineare Optimierung]]
-[[Kurseinheit 6 Verfahren zur Nichtlineare Optimierung]]
-[[Kurseinheit 7 Lineare Optimierung]]

-[[Notationen]]