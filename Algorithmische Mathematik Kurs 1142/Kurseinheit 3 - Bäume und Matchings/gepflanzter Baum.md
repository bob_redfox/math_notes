---
tags: KE_3 Bäume
---

## gepflanzter Baum

Ein gepflanzter Baum $(T, r, p)$ ist ein [[Wurzelbaum ]] an dem jeder Knoten $v \in V$ gibt es eine Reihenfolge $p(v)$ der direkten Nachfahren vorgegeben ist.

- ein gepflanzter Baum ist wohlgeklammert