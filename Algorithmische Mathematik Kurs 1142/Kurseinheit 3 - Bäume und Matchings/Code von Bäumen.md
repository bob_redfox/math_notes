---
tags: KE_3 Bäume
---

### Code von gepflanzten Bäumen

- Alle Blätter([[Blatt]]) haben den Code $()$
- Ist $x$ ein Knoten mit Kindern in der Reihenfolge $y_1, y_2, ... , y_k$, deren Codes $C_1, ... , C_k$ sind, so erhält $x$ den Code $(C_1C_2...C_k)$

- [[Wohlgeklammert]]

### Code von Wurzelbäumen

- Alle Blätter([[Blatt]]) haben den Code $()$
- Ist $x$ ein Knoten mit Kindern, deren Codes bekannt sind, so sortiere die Kinder so zu
  $y_1,...y_k$, dass für die zugehörigen Codes gilt
  $$C_1 \preceq C_2 \preceq ... \preceq C_k $$
  Dazu sortiert man die Codes lexikographisch
- $x$ erhält dann den Code $(C_1C_2...C_k)$