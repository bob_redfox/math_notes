---
tags: KE_3 Bäume Isomorphismus
---

## Isomorphismus von Bäumen

- [[Isomorphismen von gepflanzten Bäumen]]
- [[Isomorphisen von Wurzelbäumen]]

### Isomorphismus von Bäumen

- Wurzelwahl:
	- Wir suchen zunächst den Knoten der unter Isomorphismen fix bleibt.
	- Dazu nutzt man die [[Exzentrizität]]
	- Das Zentrum ist die Menge der Knoten minimaler Exzentrizität [[Zentrum]]
		- Wenn das Zentrum nur ein Knoten ist, wählt man diesen als Wurzel ansonsten:
		Sei $T = (V,E)$ ein Baum. Dann ist $|Z(T)| \leq 2$. Ist $Z(T) = {x,y}$ mit $x \neq y$, so ist $(x,y) \in E$.
	- Sollten es zwei Knoten sein, wählen trennen wir die Beiden "Wurzeln" und bestimmen die lexikographische Ordnung der Teilbäume.


## Zwei Bäume haben dann den gleichen Code, wenn sie isomorph sind.