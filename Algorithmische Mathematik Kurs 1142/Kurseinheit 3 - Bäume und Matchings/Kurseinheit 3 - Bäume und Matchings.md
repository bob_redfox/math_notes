---
tags: KE_3 Bäume Matchings Inhaltsverzeichnis
---

## Bäume und Matchings
### Definitionen
- [[Baum]]
- [[Blatt]]
- [[Code von Bäumen]]
- [[Wurzelbaum]]
- [[gepflanzter Baum]]
### Anderer Kram
- [[Isomorphismus bei Bäumen]]
- [[Aufspannende Bäume]]
- [[Bipartites Matching]]