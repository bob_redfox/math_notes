---
tags: KE_3 Aufspannende_Bäume
---

## Aufspannende Bäume

Sei $G = (V,E)$ ein Graph und $T = (V,F)$ ein Teilgraph, bei dem die Zusammenhangskomponenten die gleichen Knotenmengen wie die Zusammenhangskomponenten von G haben. 
=> $T$ ist $G$ aufspannend.
Ist $T$ dazu noch kreisfrei
=> $T$ ist ein $G$ aufspannender [[Wald]] oder ein Gerüst von $G$
Ist $G$ dazu noch zusammenhängend und $T$ ein Baum
=> $T$ ist ein $G$ aufspannender Baum

## Minimale aufspannende Bäume

