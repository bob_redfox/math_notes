---
tags: KE_3 Bäume
---

## Baum

Ein zusammenhängender Graph, der keinen Kreis enthält, $T = (V,E)$

Jeder Baum mit mindestens zwei Knoten hat mindestens zwei Blätter

Wenn wir ein Blatt vom Baum entfernen oder hinzufügen, so ist es immer noch ein Baum.
Also $T = (V, E)$ ist ein Graph und v ein Blatt des Baumes. Dann ist $T$ ein Baum genau dann, wenn $T$\ $v$  ein Baum ist.

Weitere Eigenschaften die äquivalent sind:
a) $T$ ist ein Baum
b) Zwischen je zwei Knoten $v,w \in V$ gibt es genau einen Weg von $v$ nach $w$
(Denn $T$ enthält schließlich keinen Kreis)
c) $T$ ist zusammenhängend und für alle $e \in E$ ist $T$ \ $e$ unzusammenhängen
d) $T$ ist kreisfrei
e) $T$ ist zusammenhängend und $|E| = |V| - 1$
f) $T$ ist kreisfrei und $|E| = |V| -1$
