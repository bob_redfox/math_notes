---
tags: KE_3 Bäume
---

## Isomorphismen von gepflanzten Bäumen

Sei $(T, r, \rho)$ ein gepflanzter Baum. Dann ist ein Baum isomorph, wenn er den gleichen [[Code von Bäumen]] hat.

Ein gepflanzter Baum ist immer [[Wohlgeklammert]]