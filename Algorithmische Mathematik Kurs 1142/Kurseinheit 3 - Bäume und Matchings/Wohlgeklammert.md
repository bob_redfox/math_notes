---
tags: KE_3 Bäume
---

## Wohlgeklammert

Ein Code $C$ eines Baumes ist wohlgeklammert, wenn es:

- Gleich viele öffende sowie schließende Klammern gibt
- Wenn $C$ mit einer öffnenden Klammer beginnt und einer schließenden endet