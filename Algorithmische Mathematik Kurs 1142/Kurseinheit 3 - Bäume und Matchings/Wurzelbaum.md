---
tags: KE_3 Bäume
---

## Wurzelbaum

auch genannt Arboreszenz

ist ein Paar (T, r) bestehend aus einem Baum $T$ und einem ausgezeichnetem Knoten $r \in V$

$r$ ist dabei der [[Wurzelknoten]].

Im Wurzelbaum denken wir uns dann, dass alle Kanten gerichtet von r zu allen anderen Knoten sind.


