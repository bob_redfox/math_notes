---
tags: KE_3 Matching
---

## Bipartites Matching

- [[Bipartite Graph]]
	- Ein Graph $G = (W,E)$ ist bipartit genau dann, wenn er keinen Kreis ungerader Länge hat.