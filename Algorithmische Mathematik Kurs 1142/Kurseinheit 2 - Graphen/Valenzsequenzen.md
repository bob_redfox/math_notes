---
tags: KE_2 Graphen
---

## Valenzsequenzen

Die Valenzsequenz eines Graphen ist die Anordnung der Valenzen der einzelnen Knoten $G$, auch als $deg(v)$ angegeben.

Die Valenzsequenze ist dann $(deg(v_1), deg(v_2),...,deg(v_G))$.

Nach dem [[Handshake-Lemma]] muss die Summe der Valenzen gerade sein, sonst ist es keine Valenzsequenz.
Um dann anschließend zu zeigen, dass es eine Valenzsequenz eines Graphen ist, wird das [[Verfahren nach Havel und Hakimi]] benutzt