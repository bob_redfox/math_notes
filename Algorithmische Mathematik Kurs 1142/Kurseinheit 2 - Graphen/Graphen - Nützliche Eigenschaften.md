---
tags: KE_2 
---

## Nützliche Eigenschaften von Graphen

- In jedem Graphen oder Multigraphen ist die Anzahl der Knoten mit ungeradem Knotengrad gerade. (Kollar 3.9.2) Siehe auch [[Handshake-Lemma]]