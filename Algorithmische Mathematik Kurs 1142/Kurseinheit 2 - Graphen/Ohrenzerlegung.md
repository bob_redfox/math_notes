---
tags: KE_2 Graphen Ohrenzerlegung
---

## Ohrenzerlegung

Man startet mit einem Kreis $C_0$ und bildet dann Pfade $P_1 , P_2, ...$ sodass alle Kanten und Knoten von dem Graphen erfasst werden.

Eigenschaften:
- Zeigt, dass ein Graph [[2-zusammenhängend]] ist, wenn er eine Ohrenzerlegung hat
- 