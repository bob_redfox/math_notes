---
tags: KE_2, Graphen
---

## Graphen

- [[Relationen]]
- [[Partialordnungen]] 
- [[Definition Graphen]]
	- [[Ecken]]
	- [[Kanten]]
	- [[Isomorphismus]]
	- [[Graphen - Nützliche Eigenschaften]]
- [[Wichtige Arten von Graphen]]
	- [[Vollständiger Graph]]
	- [[Zyklische Graph]]
	- [[Weg mit Knoten]]
	- [[vollständige bipartite Graph]]
	- [[Stern]]
- [[Multigraphen]]
- [[Teilgraphen]]
- [[Kodierung von Graphen]]
	- [[Adjazenzmatrix]]
- [[Breitensuche]]
- [[Tiefensuche]]
- [[Valenzsequenzen]]
- [[Eulertouren]]
- [[Ohrenzerlegung]]