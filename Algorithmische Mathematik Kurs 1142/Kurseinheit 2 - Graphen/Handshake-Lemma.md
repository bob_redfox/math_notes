---
tags: KE_2 Valenzsequenz
---

## Handshake-Lemma (Proposition 3.9.1)
In jedem (Multi-)Graphen $G = (V,E)$ ist die Summe der Knotengrade gerade, also:

$$\sum_{v\in V}deg(v)=2|E|$$


