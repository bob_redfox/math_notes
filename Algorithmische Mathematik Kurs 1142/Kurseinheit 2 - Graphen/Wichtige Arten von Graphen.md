---
tags: KE_2
---

## "Wichtige" Arten von Graphen

- $K_n$, der [[Vollständiger Graph]] mit n Knoten. Hat die Knotenmenge V und eine Kantenmenge von $\begin{pmatrix}V\\2 \end{pmatrix}$
- Sei $n\ge 3$. $C_n$ beschreibt dann den Kreis mit n Knoten. Hat eine Knotenmenge V und die Anzahl der Kanten beträgt n. [[Zyklische Graph]]
- $P_n$, der Weg mit n Knoten. Hier ist die Knotenmenge V und die Anzahl der Kanten n-1. [[Weg mit Knoten]]
- $K_{m,n}$ ist der vollständige, bipartite Graph mit m+n Knoten. Wobei hier die Knoten aus der Menge M Kanten mit allen Knoten aus der Menge N haben und umgekehrt. [[vollständige bipartite Graph]]
- [[Stern]]