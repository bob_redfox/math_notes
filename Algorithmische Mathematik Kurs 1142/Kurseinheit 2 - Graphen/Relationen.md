---
tags: KE_2 Graphen Relationen
---

### Relationen

- [[reflexiv]]
- [[symmetrisch]]
- [[transitiv]]
- [[antisymmetrisch]]
- [[Äquivalenzrelation]]