---
tags: KE_2
---

## Eulertouren von Multigraphen

Sie $G = (V,E)$ ein Multigraph. Dann sind paarweise äquivalent:

- a) $G$ ist eulersch
- b) $G$ ist zusammenhängend und alle Knoten haben geraden Knotengrad
	> Wenn ein Graph mindestens einen Knoten hat, der ungeraden Knotengrad hat, so ist der Graph nicht eulersch.
- c) $G$ ist zusammenhängend und E ist eine kantendisjunkte Vereinigung von Kreisen.

## Eulertouren von gerichteten Graphen

