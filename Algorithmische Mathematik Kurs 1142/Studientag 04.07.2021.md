# Studientag 04.07.2021

## Numerische Verfahren der nicht-linearen Optimierung [[Kurseinheit 6 Verfahren zur Nichtlineare Optimierung]]

- Ein generisches Verfahren
	- Line Search (Schrittweite)
	- Abstiegsrichtung
	Starte in $x_0 \in S$
	$k:=1$
- Algorithmen zur Bestimmung von Lösungen
- Unimodale Funktionen
	- Abstiegsrichtung macht das multidimensionale Problem zu einem eindimensionales Problem
- [[Konvexität]] -> Strikt konvex => strikt unimodal
	- Strikt konvex => jedes lokale Opt Minimum ist globales
- [[Goldener Schnitt]]
- [[Abstiegsrichtung]]
	- Koordinatensuche: Suche $x_i$ zyklisch für i = 1,...,n -> leicht zu implementieren

## [[Kurseinheit 7 Lineare Optimierung]]

- ### Lineares Programm (LP) in Standardform
	 max $c^\top x$
	 so dass Ax = b
	$x \ge 0$
	mit $b \ge 0$
	- Umformung zur Standardform
		- min $c^\top x$ -> -max$(c-)^\top x$
		- $b_i < 0$ 
		- beliebiges $x_i$ -> dann wird die Variable aufgespalten zu, $x_i^+ - x_i^-$

- ### Dualität
	- primales Program <-> duales Programm
	- x heißt zulässig für (P) falls x die Nebenbedingungen erfüllt, d.h.
	- (P) heißt zulässig. wenn es ein zulässiges x für (P) gibt
	- (P) heißt beschränkt, wenn der optimale Zielfunktionswert endlich ist.
	- Analog definiert man dieser Begriffe für (D)
	- [[Schwache Dualität]]
	- [[Starke Dualität]]
	- [[Dualitätssatz der Linearen Programmierung]]
- ### Der Simplexalgorithmus

Klausur:
	Fibbonaccisuche - evtl. was anderes ^^
	
	