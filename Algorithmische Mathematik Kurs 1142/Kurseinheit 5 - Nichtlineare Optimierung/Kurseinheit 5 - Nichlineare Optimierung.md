---
tags: KE_5, nichtlineare_Optimierung
---

## Nichtlineare Optimierung


- [[Lokale Minima bei Funktionen mit einer Variablen]]
- [[Partielle und totale Ableitung]]
	- [[Gradienten]]
	- [[Hessematrix]]
- [[Notwendinge und hinreichende Bedingungen für Extremwerte]]
- [[Mannigfaltigkeiten]]
- [[Tangentialräume]]