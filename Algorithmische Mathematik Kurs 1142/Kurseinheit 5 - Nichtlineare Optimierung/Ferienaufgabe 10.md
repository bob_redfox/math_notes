$$ y = 4 - x - z$$
Eingesetzt wird aus dem Problem => $$min \ x^2 - x - 2z + 4$$
$$unter \ x^2 + z \leq 3$$
=> Es folgen daraus folgende Funktionen
$f(x, z) = x^2 - x + 2z + 4$
$g(x, z) = x^2 + z - 3 \leq 0$
Dann bestimmt man die Gradienten der beiden Funktionen
$\nabla f(x,z) = (2x-1, -2)$
$\nabla g(x,z) =(2x, 1)$

Dann sucht man nach kritischen Punkten im Inneren sowie den Rändern.
Also ist es möglich, dass $\nabla f(x,z)=(0,0)$ werden kann.

$\mu$