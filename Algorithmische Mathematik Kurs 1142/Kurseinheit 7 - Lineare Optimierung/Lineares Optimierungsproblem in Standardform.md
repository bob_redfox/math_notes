---
tags: lineare_Optimierung, KE_7
---

Lineare Optimierungsprobleme werden am liebsten als Probleme gesehen, wo man den maximalen Wert bestimmen möchte.
Dafür gibt es die Standardform, auch [[Primales Programm]] genannt mit :
$$\text{max}\ c^\top x$$
$$\text{unter}\ Ax=b$$
$$x \geq 0$$

Um Minimierungsprobleme zu lösen muss man das Minimierungsproblem wie folgt zu einem Maximierungsproblem "umformen":

$$\underset{x\in S} {max}\ c(x)=-(\underset{x \in S}{min}\ -c(x))$$

Nebenbedingungen mit $b\ge 0$ können mit $-1$ multipliziert werden um $a^{\top}x \le \beta$ zu erreichen.

Um Ungleichungen zu beseitigen werden [[Schlupfvariablen]] eingebaut.

Beispiel um Minimierungsprobleme zu Maximierungsprobleme umzuwandeln:

$$min\ f(x) =  -max  (-f(x))$$