---
tags: KE_7 lineare_Optimierung Inhaltsverzeichnis
---

## Lineare Optimierung

-[[Modellbildung]]
-[[Lineares Optimierungsproblem in Standardform]]
-[[Primales Programm]]
-[[Duales Programm]]
-[[Dualitätssatz der Linearen Programmierung]]
-[[Simplexalgorithmus]]


Beschreibt Verfahren für lineare Zielfunktionen mit linearen Nebenbedingungen. Hierbei wird die Modellbildung, der Dualitätssatz und das Simplex-Verfahren näher beleuchtet.