---
tags: Standardform, KE_7, lineare_Optimierung
---
## Schlupfvariablen

Schlupfvariabeln dienen dazu um Ungleichungen in der linearen Optimierung zu Gleichungen umzuformen.
Dazu sei $Ax \ge b$ eine Nebenbedinung mit $x \ge 0$. Dann kann man die Schlupfvariable $z = b - Ax$ mit $z \ge 0$ einbauen. Sodass zu Schluss:
$$Ax + z = b$$ ist mit $x, z \ge 0$