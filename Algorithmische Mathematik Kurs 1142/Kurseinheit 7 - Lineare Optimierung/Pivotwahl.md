---
tags: KE_7 lineare_Optimierung Simplex
---

## Pivotwahl

Auswahl der zu entsprechenden Spalte und Zeile im Simplex um dem Optimieren.

Zunächst suche man einen Wert bei den reduzierten Kosten, welche positiv sind!
Dabei gibt es ein paar Strategien:

- [[Steilster Anstieg]]
- [[Bland's rule]]