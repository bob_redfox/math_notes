---
tags: lineare_Optimierung, KE_7
---

Lineare Optimierungsprobleme werden als Probleme gesehen, wo man den maximalen Wert raus bekommt.
Dafür gibt es die Standardform, auch [[Primales Programm]] genannt mit :
$$\text{max}\ c^\top x$$
$$\text{unter}\ Ax=b$$
$$x \geq 0$$