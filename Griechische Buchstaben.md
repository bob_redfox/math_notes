---
tags: Notation
---


## Griechische Buchstaben

|klein|groß|Name|Latex|Latex| 
|:---:|:---:|:--:|:--:|:--:|
| $\alpha$ | $A$ | alpha | \alpha | A
| $\beta$ | $B$| beta | \beta | B
| $\gamma$ | $\Gamma$ | gamma | \gamma | \Gamma
| $\delta$ | $\Delta$ | delta | \delta | \Delta
| $\epsilon$ | $E$ | epsilon | \epsilon | E
| $\zeta$ | $Z$ | zeta | \zeta | Z
| $\rho$ | | rho | \rho

